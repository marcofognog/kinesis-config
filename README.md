# Setup

Hold `progm` + `shift` and tap `esc` for **PUM** (Power User Mode)

hold `progm` and tap `F1` to enter **v-drive mode**

Copy files `1_querty.txt` and `2_querty.txt` into keyboard (in the "active" folder)

Hold `progm` and tap `F1` to exit v-drive mode

Hold `progm` + `shift` and tap `esc` to exit PUM

Hold `progm` tap `2` to select the 2_querty.txt layout (my custom querty) or `3` to select the 3_querty.txt (steno)

For custom querty, on ubuntu run `setxkbmap br` on cli to set brazilian layout

Enjoy.
