* Stenography layout
* Have to use numbers to differenciate between the left letter form the right letters,
* than remap them via software (Plover)
*
* First left hand row
[Q]>[1] * 1 => S-
[W]>[2] * 2 => T-
[E]>[3] * 3 => P-
[R]>[4] * 4 => H-
{T}>{-rshift}{8}{+rshift}
*
* Second left hand row (combination)
[A]>[1]
{S}>{2}{5} * TK
{D}>{3}{6} * PW
{F}>{4}{7} * HR
{G}>{-rshift}{8}{+rshift}
*
* Third left hand row
[Z]>[1]
[X]>[5] * 5 => K-
[C]>[6] * 6 => W-
[V]>[7] * 7 => R-
{B}>{-rshift}{8}{+rshift}
*
* First right hand row
{Y}>{-rshift}{8}{+rshift}
[U]>[F]
[I]>[P]
[O]>[L]
[P]>[T]
[\]>[D]
*
* Second right hand row (combination)
{H}>{-rshift}{8}{+rshift}
{J}>{F}{R}
{K}>{P}{B}
{L}>{L}{G}
{;}>{T}{S}
{'}>{D}{Z}
*
* Third right hand row
{N}>{-rshift}{8}{+rshift}
[M]>[R]
[,]>[B]
[.]>[G]
[/]>[S]
[rshift]>[Z]
*
* Left thumb cluster
[BSPACE]>[A]
[DELETE]>[O]
*
* Right thumb cluster
[ENTER]>[E]
[SPACE]>[U]
